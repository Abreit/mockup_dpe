
from setuptools import setup, find_packages

setup(
    name='controle_coherence',
    packages=find_packages(),
    version='0.1.0',
    url =  'https://gitlab.com/Abreit/controle_coherence')
