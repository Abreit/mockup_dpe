from flask import Flask
from flask import request, jsonify
from flask_swagger_ui import get_swaggerui_blueprint
from controle_coherence.validation import procedure_validation
import json
import lxml
from lxml import etree

app = Flask(__name__)


def load_xml_object(request):
    error = None
    xml_tree = None
    if request.data:

        try:
            xml_tree = etree.ElementTree(etree.fromstring(request.data))
        except Exception:
            error = f"""
                    impossible de lire le fichier xml envoyé de taille : {len(request.data)}.\n' 
                    Fichier : \n
                    {request.data[0:100]}
                   """
    else:
        error = 'no data provided'
    return xml_tree, error

SWAGGER_URL = '/api'
API_URL = '/static/api.yml'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "controle_coherence"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

@app.route('/')
def homepage():

    return 'bienvenue sur le webservice controle de cohérence DPE'

@app.route("/controle_coherence", methods=['POST'])
def controle_coherence():
    # charger xml
    xml_tree, error = load_xml_object(request)
    if error is not None:
        return error, 400

    # procedure de validation
    resp = procedure_validation(xml_tree)

    return jsonify(resp), 200


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=5000)
