from lxml import etree
from pathlib import Path

xml_validator = etree.XMLSchema(file=str(Path(__file__).parent / "DPE.xsd"))


def validate(xml_tree, xml_validator=xml_validator):
    resp = xml_validator.validate(xml_tree)

    return {"valid": resp,
            "error_log": str(xml_validator.error_log)}


def procedure_validation(xml_tree):
    resp = dict()
    # validation XSD
    resp_validate = validate(xml_tree)
    resp['validation_xsd'] = resp_validate
    # controle de cohérence
    resp['controle_coherence'] = {
        'erreur_logiciel': [
            {"message": "un message d'erreur typique sur le système de chauffage",
             "thematique": "systeme_chauffage",
             "objets_concerne": ['/dpe/logement/enveloppe/mur_collection/mur[1]',
                        '/dpe/logement/enveloppe/mur_collection/mur[2]'],

             }
        ],
        'erreur_saisie': [],
        'warning_logiciel': [],
        'warning_saisie': [{"message": "un message d'erreur typique sur le système de chauffage",
                            "thematique": "systeme_chauffage",
                            "objets_concerne": ['/dpe/logement/enveloppe/mur_collection/mur[1]',
                                       '/dpe/logement/enveloppe/mur_collection/mur[2]'],

                            }],
    }
    return resp
